pub mod setup;
pub mod state;
pub mod update;

use self::{
    state::ShootWeaponEvent,
    update::{trigger_weapons, update_weapons},
};

use super::player::update::update as player_update;
use bevy::prelude::*;
use bevy_common_assets::ron::RonAssetPlugin;
use setup::PlayerConfig;

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app
        .add_plugins(RonAssetPlugin::<PlayerConfig>::new(&["player.ron"]))
        .add_event::<ShootWeaponEvent>().add_systems(
            Update,
            (player_update, trigger_weapons, update_weapons).chain(),
        );
    }
}
