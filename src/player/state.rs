use core::fmt;
use std::collections::HashMap;

use bevy::prelude::{Bundle, Component, Event, Handle, Vec2};
use micro_banimate::definitions::AnimationSet;
use serde::Deserialize;

use crate::common::animated_sprite_state::{Action, AnimatedSpriteState, CollisionState};

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Deserialize)]
pub enum PlayerAnimationSet {
    Naked,
    IronArmor,
    GoldArmor,
}

impl fmt::Display for PlayerAnimationSet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            PlayerAnimationSet::Naked => write!(f, "naked"),
            PlayerAnimationSet::IronArmor => write!(f, "iron_armor"),
            PlayerAnimationSet::GoldArmor => write!(f, "gold_armor"),
        }
    }
}

#[derive(Component, Clone)]
pub struct PlayerState {
    pub state: AnimatedSpriteState,
    pub animation_sets: HashMap<PlayerAnimationSet, Handle<AnimationSet>>,
    pub current_animation_set: PlayerAnimationSet,
    pub current_weapon_type: WeaponType,
    pub v_jump_acceleration: f32,
    pub direction: Vec2,
}

impl PlayerState {
    pub fn get_animation_set_handle(
        &self,
        animation_set: &PlayerAnimationSet,
    ) -> Handle<AnimationSet> {
        match self.animation_sets.get(animation_set) {
            Some(handle) => handle.clone(),
            None => self.animation_sets.values().next().unwrap().clone(),
        }
    }

    pub fn get_current_animation_set_handle(&self) -> Handle<AnimationSet> {
        self.get_animation_set_handle(&self.current_animation_set)
    }
}

impl Default for PlayerState {
    fn default() -> Self {
        Self {
            state: AnimatedSpriteState {
                current_action: Action::None,
                velocity_increment: Vec2::new(20.0, 5.0),
                min_velocity: Vec2::new(20.0, 5.0),
                max_velocity: Vec2::new(200.0, 30.0),
                ..AnimatedSpriteState::default()
            },
            animation_sets: HashMap::new(),
            current_animation_set: PlayerAnimationSet::IronArmor,
            current_weapon_type: WeaponType::Spear,
            v_jump_acceleration: 350.0,
            direction: Vec2::ZERO,
        }
    }
}

#[derive(Bundle)]
pub struct PlayerBundle {
    pub state: PlayerState,
    //pub current_animation: Animation
}

impl Default for PlayerBundle {
    fn default() -> Self {
        Self {
            state: PlayerState::default(),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum WeaponType {
    Spear,
    Knife,
    Axe,
    Sword,
    Fire,
}

impl fmt::Display for WeaponType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            WeaponType::Spear => write!(f, "spear"),
            WeaponType::Knife => write!(f, "knife"),
            WeaponType::Axe => write!(f, "axe"),
            WeaponType::Sword => write!(f, "sword"),
            WeaponType::Fire => write!(f, "fire"),
        }
    }
}

#[derive(Event)]
pub struct ShootWeaponEvent {
    pub weapon_type: WeaponType,
    pub start_position: Vec2,
    pub direction: Vec2,
}

#[derive(Component, Clone)]
pub struct WeaponState {
    pub current_collision_state: CollisionState,
    pub velocity_increment: f32,
    pub max_velocity: f32,
    pub min_velocity: f32,
    pub direction: Vec2,
}

impl WeaponState {
    pub fn spear() -> Self {
        Self {
            current_collision_state: CollisionState::NoColliding,
            velocity_increment: 50.0,
            max_velocity: 1000.0,
            min_velocity: 100.0,
            direction: Vec2::new(1.0, 0.25).normalize(),
        }
    }

    pub fn knife() -> Self {
        Self {
            current_collision_state: CollisionState::NoColliding,
            velocity_increment: 75.0,
            max_velocity: 1300.0,
            min_velocity: 200.0,
            direction: Vec2::new(1.0, 0.25).normalize(),
        }
    }

    pub fn axe() -> Self {
        Self {
            current_collision_state: CollisionState::NoColliding,
            velocity_increment: 50.0,
            max_velocity: 800.0,
            min_velocity: 100.0,
            direction: Vec2::new(1.0, 0.5).normalize(),
        }
    }

    pub fn fire() -> Self {
        Self {
            current_collision_state: CollisionState::NoColliding,
            velocity_increment: 50.0,
            max_velocity: 800.0,
            min_velocity: 100.0,
            direction: Vec2::new(1.0, 0.75).normalize(),
        }
    }

    pub fn sword() -> Self {
        Self {
            current_collision_state: CollisionState::NoColliding,
            velocity_increment: 0.0,
            max_velocity: 0.0,
            min_velocity: 0.0,
            direction: Vec2::ZERO,
        }
    }
}

#[derive(Bundle)]
pub struct WeaponBundle {
    pub state: WeaponState,
}
