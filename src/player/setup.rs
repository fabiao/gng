use crate::common::{
    animated_sprite_state::CollisionLayer,
    game_layer::{DrawLayer, get_z_index},
};

use avian2d::{math::Scalar, prelude::*};
use bevy::prelude::*;
use micro_banimate::{
    definitions::{AnimationHandle, AnimationStatus, SpriteAnimation},
    directionality::Directionality,
};

use super::state::{PlayerAnimationSet, PlayerState, WeaponState, WeaponType};

type PlayerPreset = (
    Transform,
    Visibility,
    Sprite,
    SpriteAnimation,
    Directionality,
    AnimationHandle,
    AnimationStatus,
    PlayerState,
    RigidBody,
    Collider,
    CollisionLayer,
    LinearVelocity,
    Mass,
    LockedAxes,
    Friction,
);

type WeaponPreset = (
    Transform,
    Visibility,
    Sprite,
    SpriteAnimation,
    AnimationHandle,
    AnimationStatus,
    WeaponState,
    RigidBody,
    Collider,
    CollisionLayer,
    LinearVelocity,
    Mass,
    Sensor,
);

#[derive(serde::Deserialize, Asset, TypePath)]
pub struct PlayerConfig {
    pub image_file_name: String,
    pub tile_size: UVec2,
    pub columns: u32,
    pub rows: u32,
    pub padding: Option<UVec2>,
    pub offset: Option<UVec2>,
    pub animation_sets: Vec<(PlayerAnimationSet, String)>,
    pub start_animation_name: String,
}

#[derive(Resource)]
struct PlayerConfigHandle(Handle<PlayerConfig>);

pub fn create_player_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    position: &Vec3,
    mass: Option<Scalar>,
) -> PlayerPreset {
    let _level = PlayerConfigHandle(asset_server.load("player.ron"));
    //commands.insert_resource(level);

    let player_config = PlayerConfig {
        image_file_name: "sprites/player.png".to_string(),
        tile_size: UVec2::new(46, 46),
        columns: 26,
        rows: 13,
        padding: None,
        offset: None,
        animation_sets: vec![
            (
                PlayerAnimationSet::Naked,
                "animations/player/naked.anim.json".to_string(),
            ),
            (
                PlayerAnimationSet::IronArmor,
                "animations/player/iron_armor.anim.json".to_string(),
            ),
            (
                PlayerAnimationSet::GoldArmor,
                "animations/player/gold_armor.anim.json".to_string(),
            ),
        ],
        start_animation_name: "idle".to_string(),
    };

    let image_handle = asset_server.load(player_config.image_file_name);
    let texture_atlas_layout = TextureAtlasLayout::from_grid(
        player_config.tile_size,
        player_config.columns,
        player_config.rows,
        player_config.padding,
        player_config.offset,
    );
    let texture_atlas_layout_handle = texture_atlas_layouts.add(texture_atlas_layout);
    let sprite = Sprite::from_atlas_image(
        image_handle,
        TextureAtlas::from(texture_atlas_layout_handle),
    );

    let mut player_state = PlayerState::default();

    for (animation_set, animation_file_name) in player_config.animation_sets {
        player_state
            .animation_sets
            .insert(animation_set, asset_server.load(animation_file_name));
    }

    player_state.current_animation_set = PlayerAnimationSet::IronArmor;

    player_state.current_weapon_type = WeaponType::Spear;

    let mass = if let Some(mass) = mass {
        Mass(mass)
    } else {
        Mass(100.0)
    };

    (
        Transform::from_translation(*position),
        Visibility::Visible,
        sprite,
        SpriteAnimation {},
        Directionality::Left,
        AnimationHandle(player_state.get_current_animation_set_handle()),
        AnimationStatus::new(player_config.start_animation_name),
        player_state,
        RigidBody::Dynamic,
        Collider::capsule(18.0, 12.0),
        CollisionLayer::Player,
        LinearVelocity::ZERO,
        mass,
        LockedAxes::ROTATION_LOCKED,
        Friction::new(0.2)
    )
}

pub fn create_weapon_preset(
    weapon_type: WeaponType,
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec2,
    direction: &Vec2,
    tile_size: &UVec2,
    (columns, rows): (u32, u32),
    collider: Collider,
    mass: Option<Scalar>,
) -> WeaponPreset {
    let weapon_name = weapon_type.to_string();
    let mut weapon_state = match weapon_type {
        WeaponType::Spear => WeaponState::spear(),
        WeaponType::Knife => WeaponState::knife(),
        WeaponType::Axe => WeaponState::axe(),
        WeaponType::Fire => WeaponState::fire(),
        WeaponType::Sword => WeaponState::sword(),
    };
    let max_velocity = weapon_state.max_velocity;
    let weapon_direction = weapon_state.direction;
    let final_direction = Vec2::new(
        /*weapon_direction.x **/ direction.x.signum(),
        weapon_direction.y,
    ) * max_velocity;
    weapon_state.direction = *direction;

    let image_handle = asset_server.load(String::from("sprites/") + weapon_name.as_str() + ".png");
    let texture_atlas_layout = TextureAtlasLayout::from_grid(*tile_size, columns, rows, None, None);
    let texture_atlas_layout_handle = texture_atlas_layouts.add(texture_atlas_layout);
    let sprite = Sprite::from_atlas_image(
        image_handle,
        TextureAtlas::from(texture_atlas_layout_handle),
    );

    let animation_handle = asset_server.load("animations/player/weapons.anim.json");

    let mass = if let Some(mass) = mass {
        Mass(mass)
    } else {
        Mass(100.0)
    };

    (
        Transform::from_xyz(
            start_position.x,
            start_position.y,
            get_z_index(DrawLayer::Object),
        ),
        Visibility::Visible,
        sprite,
        SpriteAnimation {},
        AnimationHandle(animation_handle),
        AnimationStatus::new(weapon_name),
        weapon_state,
        RigidBody::Dynamic,
        collider,
        CollisionLayer::Weapon,
        LinearVelocity::from(final_direction),
        mass,
        Sensor,
    )
}

pub fn create_spear_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec2,
    direction: &Vec2,
) -> WeaponPreset {
    create_weapon_preset(
        WeaponType::Spear,
        asset_server,
        texture_atlas_layouts,
        start_position,
        direction,
        &UVec2::new(42, 9),
        (2, 1),
        Collider::rectangle(21.0, 5.0),
        Some(30.0),
    )
}

pub fn create_knife_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec2,
    direction: &Vec2,
) -> WeaponPreset {
    create_weapon_preset(
        WeaponType::Knife,
        asset_server,
        texture_atlas_layouts,
        start_position,
        direction,
        &UVec2::new(25, 9),
        (2, 1),
        Collider::rectangle(13.0, 5.0),
        Some(20.0),
    )
}

pub fn create_axe_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec2,
    direction: &Vec2,
) -> WeaponPreset {
    create_weapon_preset(
        WeaponType::Axe,
        asset_server,
        texture_atlas_layouts,
        start_position,
        direction,
        &UVec2::new(36, 36),
        (8, 1),
        Collider::rectangle(18.0, 18.0),
        Some(20.0),
    )
}

pub fn create_fire_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec2,
    direction: &Vec2,
) -> WeaponPreset {
    create_weapon_preset(
        WeaponType::Fire,
        asset_server,
        texture_atlas_layouts,
        start_position,
        direction,
        &UVec2::new(22, 22),
        (12, 1),
        Collider::rectangle(11.0, 11.0),
        Some(20.0),
    )
}

pub fn create_sword_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec2,
    direction: &Vec2,
) -> WeaponPreset {
    create_weapon_preset(
        WeaponType::Sword,
        asset_server,
        texture_atlas_layouts,
        start_position,
        direction,
        &UVec2::new(48, 48),
        (4, 1),
        Collider::rectangle(22.0, 22.0),
        None,
    )
}
