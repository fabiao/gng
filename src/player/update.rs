use std::time::Duration;

use avian2d::prelude::LinearVelocity;
use bevy::prelude::*;
use micro_banimate::{
    definitions::{AnimationHandle, AnimationMode, AnimationSet, AnimationStatus},
    directionality::{Directionality, Horizontal, Vertical},
};

use crate::{
    common::animated_sprite_state::{Action, Actionable, AnimationType, CollisionState, PositionChecked},
    input::state::{ButtonState, InputState},
};

use super::{
    setup::{
        create_axe_preset, create_fire_preset, create_knife_preset, create_spear_preset,
        create_sword_preset,
    },
    state::{PlayerAnimationSet, PlayerState, ShootWeaponEvent, WeaponState, WeaponType},
};

fn animate_grounded(
    (_input_moved_x, input_moved_y): (bool, bool),
    input_movement: &Vec2,
    player_state: &mut PlayerState,
    animation_status: &mut AnimationStatus,
    velocity: &mut LinearVelocity,
) {
    if !player_state.state.animation_timer.finished() {
        let velocity_x_abs = velocity.x.abs();
        let max_velocity_x = player_state.state.max_velocity.x;
        if velocity_x_abs > (max_velocity_x * 0.5) {
            animation_status.frame_time += velocity_x_abs / (max_velocity_x * 150.0);
        }
    }

    if input_moved_y && input_movement.y < 0.0 {
        if !player_state.state.check_current_action(Action::Crouched) {
            player_state.state.add_action(Action::Crouched);
        }
    } else {
        if player_state.state.check_current_action(Action::Crouched) {
            player_state.state.remove_action(Action::Crouched);
        }
    }
}

fn update_velocity(
    input_moved_x: bool,
    input_movement: &Vec2,
    velocity: &mut LinearVelocity,
    player_state: &mut PlayerState,
) {
    if input_moved_x {
        let increment_velocity = *input_movement * player_state.state.velocity_increment;
        if velocity.x.abs() + increment_velocity.x.abs() < player_state.state.max_velocity.x {
            velocity.x += increment_velocity.x;
        } else {
            velocity.x = player_state.state.max_velocity.x * player_state.direction.x;
        }
    }
}

fn update_shoot(
    shoot_state: ButtonState,
    translation: &Vec3,
    directionality: &Directionality,
    animation_status: &mut AnimationStatus,
    player_state: &mut PlayerState,
    shoot_weapon_event_writer: &mut EventWriter<ShootWeaponEvent>,
) {
    let is_playing_shooting_animation = player_state.state.check_current_action(Action::Shooting);
    if is_playing_shooting_animation {
        if animation_status.active_step == 1 && animation_status.frame_time >= 0.140 {
            player_state.state.remove_action(Action::Shooting);
        }
    } else if shoot_state == ButtonState::JustPressed {
        player_state.state.add_action(Action::Shooting);

        let current_weapon_type = player_state.current_weapon_type;
        let mut weapon_start_position = Vec2::new(translation.x, translation.y - 4.0);
        if player_state.state.check_current_action(Action::Crouched) {
            weapon_start_position.y -= 12.0;
        }
        let weapon_start_direction = if directionality.is_left() {
            Vec2::NEG_X
        } else {
            Vec2::X
        };
        player_state.state.animation_timer =
            Timer::new(Duration::from_millis(200), TimerMode::Once);
        shoot_weapon_event_writer.send(ShootWeaponEvent {
            weapon_type: current_weapon_type,
            start_position: weapon_start_position,
            direction: weapon_start_direction,
        });
        log::info!(
            "Shooting weapon \"{}\" from {} with dir {}",
            current_weapon_type,
            weapon_start_position,
            weapon_start_direction
        );
    }
}

fn update_player(
    input_state: &InputState,
    mut shoot_weapon_event_writer: &mut EventWriter<ShootWeaponEvent>,
    directionality: &mut Directionality,
    mut velocity: &mut LinearVelocity,
    transform: &mut Transform,
    mut player_state: &mut PlayerState,
    mut animation_status: &mut AnimationStatus,
    animation_mode: &mut AnimationMode,
    animation_handle: &mut Handle<AnimationSet>,
    visibility: &mut Visibility,
    sprite: &mut Sprite,
) {
    if player_state.state.check_current_action(Action::Rising) {
        animation_status.start_or_continue(AnimationType::Crouch.to_string());
        return;
    }

    if player_state.state.is_alive() {
        if player_state
            .state
            .current_collision_state
            .intersects(CollisionState::CollidingEnemy)
        {
            //let previous_animation = animation_status.active_name.clone();
            //animation_status.start_animation(AnimationType::Die.to_string());
            //*animation_mode = AnimationMode::OnceThenPlay(previous_animation);
            //commands.entity(entity).despawn();
            return;
        }

        if input_state.respawn_player {
            transform.translation = Vec3::new(-2952.0, 271.5, transform.translation.z);
            *visibility = Visibility::Visible;
            log::info!("Respawn!");
            return;
        }

        if input_state.toggle_armor {
            player_state.current_animation_set = match player_state.current_animation_set {
                PlayerAnimationSet::Naked => PlayerAnimationSet::IronArmor,
                PlayerAnimationSet::IronArmor => PlayerAnimationSet::GoldArmor,
                PlayerAnimationSet::GoldArmor => PlayerAnimationSet::Naked,
            };
            *animation_handle = player_state.get_current_animation_set_handle();
        }

        if input_state.toggle_weapon {
            player_state.current_weapon_type = match player_state.current_weapon_type {
                WeaponType::Spear => WeaponType::Knife,
                WeaponType::Knife => WeaponType::Axe,
                WeaponType::Axe => WeaponType::Fire,
                WeaponType::Fire => WeaponType::Sword,
                WeaponType::Sword => WeaponType::Spear,
            };
        }

        let input_movement = input_state.check_max_movement();
        let (input_moved_x, input_moved_y) = input_state.check_moved();
        if input_moved_x || input_moved_y {
            if input_moved_x {
                player_state.direction.x = input_movement.x.signum();
            } else {
                player_state.direction.x = 0.0;
            }
            if input_moved_y {
                player_state.direction.y = input_movement.y.signum();
            } else {
                player_state.direction.y = 0.0;
            }
            directionality.from_direction(&player_state.direction);
        }

        let player_colliding_floor = player_state
            .state
            .current_collision_state
            .intersects(CollisionState::CollidingFloor);

        if !player_state.state.check_current_action(Action::Jumping) {
            if input_state.jump_state == ButtonState::JustPressed {
                velocity.y = player_state.v_jump_acceleration;
                player_state.state.add_action(Action::Jumping);
                player_state.state.remove_action(Action::Shooting);
            }
        } else {
            if player_colliding_floor {
                player_state.state.remove_action(Action::Jumping);
            }
        }

        if player_colliding_floor && velocity.y < 200.0 {
            if !player_state.state.check_current_action(Action::Grounded) {
                player_state.state.add_action(Action::Grounded);
            }

            animate_grounded(
                (input_moved_x, input_moved_y),
                &input_movement,
                player_state,
                animation_status,
                velocity,
            );
        } else {
            if player_state.state.check_current_action(Action::Grounded) {
                player_state.state.remove_action(Action::Grounded);
            }
        }

        if !player_state.state.check_current_action(Action::Crouched) {
            update_velocity(
                input_moved_x,
                &input_movement,
                &mut velocity,
                &mut player_state,
            );
        }

        update_shoot(
            input_state.shoot_state,
            &transform.translation,
            &directionality,
            &mut animation_status,
            &mut player_state,
            &mut shoot_weapon_event_writer,
        );

        // Select animation
        if player_state.state.check_current_action(Action::Shooting) {
            if player_state.state.check_current_action(Action::Crouched) {
                animation_status.start_or_continue(AnimationType::ShootCrouched.to_string());
            } else {
                animation_status.start_or_continue(AnimationType::Shoot.to_string());
            }
            *animation_mode = AnimationMode::Once;
        } else {
            if player_state.state.check_current_action(Action::Grounded) {
                if player_state.state.check_current_action(Action::Crouched) {
                    animation_status.start_or_continue(AnimationType::Crouch.to_string());
                } else if input_moved_x || velocity.x.abs() >= player_state.state.min_velocity.x {
                    animation_status.start_or_continue(AnimationType::Walk.to_string());
                } else {
                    animation_status.start_or_continue(AnimationType::Idle.to_string());
                }
            } else {
                if player_state.state.check_current_action(Action::Jumping) {
                    animation_status.start_or_continue(AnimationType::Jump.to_string());
                } else {
                    if input_moved_y
                        && input_movement.y > 0.0
                        && player_state.state.current_action == Action::None
                    {
                        player_state.state.add_action(Action::Jumping);
                    }
                }
            }
            *animation_mode = AnimationMode::Loop;
        }

        if input_moved_x {
            sprite.flip_x = player_state.direction.x < 0.0;
        }
    }
}

pub fn update(
    input_state: Res<InputState>,
    mut shoot_weapon_event_writer: EventWriter<ShootWeaponEvent>,
    mut query: Query<(
        &mut PlayerState,
        &mut Directionality,
        &mut LinearVelocity,
        &mut Transform,
        &mut AnimationStatus,
        &mut AnimationMode,
        &mut AnimationHandle,
        &mut Visibility,
        &mut Sprite,
    )>,
) {
    query.iter_mut().for_each(
        |(
            mut player_state,
            mut directionality,
            mut velocity,
            mut transform,
            mut animation_status,
            mut animation_mode,
            mut animation_handle,
            mut visibility,
            mut sprite,
        )| {
            update_player(
                &input_state,
                &mut shoot_weapon_event_writer,
                &mut directionality,
                &mut velocity,
                &mut transform,
                &mut player_state,
                &mut animation_status,
                &mut animation_mode,
                &mut animation_handle,
                &mut visibility,
                &mut sprite,
            );
        },
    );
}

pub fn trigger_weapons(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlas_layouts: ResMut<Assets<TextureAtlasLayout>>,
    mut shoot_weapon_event_reader: EventReader<ShootWeaponEvent>,
) {
    for shoot_weapon_event in shoot_weapon_event_reader.read() {
        let weapon_position = Vec2::new(
            shoot_weapon_event.start_position.x,
            shoot_weapon_event.start_position.y + 23.0,
        );
        let bundle = match shoot_weapon_event.weapon_type {
            WeaponType::Spear => create_spear_preset(
                &asset_server,
                &mut texture_atlas_layouts,
                &weapon_position,
                &shoot_weapon_event.direction,
            ),
            WeaponType::Knife => create_knife_preset(
                &asset_server,
                &mut texture_atlas_layouts,
                &weapon_position,
                &shoot_weapon_event.direction,
            ),
            WeaponType::Axe => create_axe_preset(
                &asset_server,
                &mut texture_atlas_layouts,
                &weapon_position,
                &shoot_weapon_event.direction,
            ),
            WeaponType::Fire => create_fire_preset(
                &asset_server,
                &mut texture_atlas_layouts,
                &weapon_position,
                &shoot_weapon_event.direction,
            ),
            WeaponType::Sword => create_sword_preset(
                &asset_server,
                &mut texture_atlas_layouts,
                &weapon_position,
                &shoot_weapon_event.direction,
            ),
        };
        commands.spawn(bundle);
    }
}

pub fn update_weapons(
    mut commands: Commands,
    mut query: Query<(
        Entity,
        &WeaponState,
        &Transform,
        &mut AnimationStatus,
        &mut AnimationMode,
        &mut Sprite,
    )>,
) {
    query.iter_mut().for_each(
        |(
            entity,
            weapon_state,
            transform,
            mut animation_status,
            mut animation_mode,
            mut sprite,
        )| {
            if weapon_state
                .current_collision_state
                .intersects(CollisionState::CollidingFloor | CollisionState::CollidingEnemy)
                || transform.translation.y < -500.0
            {
                commands.entity(entity).despawn();
            } else {
                *animation_mode = AnimationMode::Loop;
                let active_name = animation_status.active_name.clone();
                animation_status.start_or_continue(active_name);

                sprite.flip_x = weapon_state.direction.x < 0.0;
            }
        },
    );
}

pub fn despawn_current_players_and_weapons(
    commands: &mut Commands,
    players_query: &Query<(Entity, &PlayerState)>,
    weapons_query: &Query<(Entity, &WeaponState)>,
) {
    players_query.iter().for_each(|(entity, _)| {
        commands.entity(entity).despawn_recursive();
    });
    weapons_query.iter().for_each(|(entity, _)| {
        commands.entity(entity).despawn_recursive();
    });
}
