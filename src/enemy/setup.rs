use crate::common::animated_sprite_state::CollisionLayer;

use avian2d::{math::Scalar, prelude::*};
use bevy::{prelude::*, transform::components::Transform};
use micro_banimate::definitions::{AnimationHandle, AnimationStatus, SpriteAnimation};

use super::state::{EnemyState, EnemyType};

type EnemyPreset = (
    Transform,
    Visibility,
    Sprite,
    SpriteAnimation,
    AnimationHandle,
    AnimationStatus,
    EnemyState,
    RigidBody,
    Collider,
    CollisionLayer,
    LinearVelocity,
    Mass,
    LockedAxes,
);

pub fn create_enemy_preset(
    enemy_type: EnemyType,
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec3,
    direction: &Vec2,
    tile_size: &UVec2,
    (columns, rows): (u32, u32),
    collider: Collider,
    mass: Option<Scalar>,
) -> EnemyPreset {
    let enemy_name = enemy_type.to_string();
    let image_handle = asset_server.load(String::from("sprites/") + enemy_name.as_str() + ".png");
    let texture_atlas_layout = TextureAtlasLayout::from_grid(*tile_size, columns, rows, None, None);
    let texture_atlas_layout_handle = texture_atlas_layouts.add(texture_atlas_layout);
    let sprite = Sprite::from_atlas_image(
        image_handle,
        TextureAtlas::from(texture_atlas_layout_handle),
    );
    
    let enemy_state = match enemy_type {
        EnemyType::Skeleton => EnemyState::skeleton(),
        EnemyType::Zombie => EnemyState::zombie(),
        EnemyType::Ghost => EnemyState::ghost(),
        EnemyType::RedArremerKing => EnemyState::red_arremer_king(),
    };
    let enemy_name_cloned = enemy_name.clone();
    let mass = if let Some(mass) = mass {
        Mass(mass)
    } else {
        Mass(100.0)
    };
    
    let animation_handle = asset_server
        .load(String::from("animations/enemies/") + enemy_name_cloned.as_str() + ".anim.json");

    (
        Transform::from_translation(*start_position),
        Visibility::Visible,
        sprite,
        SpriteAnimation {},
        AnimationHandle(animation_handle),
        AnimationStatus::new(enemy_name),
        enemy_state,
        RigidBody::Dynamic,
        collider,
        CollisionLayer::Enemy,
        LinearVelocity::from(*direction),
        mass,
        LockedAxes::ROTATION_LOCKED,
        //collider_debug_color: ColliderDebugColor(Color::YELLOW),
    )
}

pub fn create_skeleton_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec3,
) -> EnemyPreset {
    create_enemy_preset(
        EnemyType::Skeleton,
        asset_server,
        texture_atlas_layouts,
        start_position,
        &Vec2::ZERO,
        &UVec2::new(46, 46),
        (26, 13),
        Collider::capsule(16.0, 18.0),
        None,
    )
}

pub fn create_zombie_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec3,
) -> EnemyPreset {
    create_enemy_preset(
        EnemyType::Zombie,
        asset_server,
        texture_atlas_layouts,
        start_position,
        &Vec2::ZERO,
        &UVec2::new(46, 46),
        (26, 13),
        Collider::capsule(24.0, 18.0),
        None,
    )
}

pub fn create_ghost_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec3,
) -> EnemyPreset {
    create_enemy_preset(
        EnemyType::Ghost,
        asset_server,
        texture_atlas_layouts,
        start_position,
        &Vec2::ZERO,
        &UVec2::new(46, 46),
        (26, 13),
        Collider::capsule(24.0, 18.0),
        Some(0.0),
    )
}

pub fn create_red_arremer_king_preset(
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
    start_position: &Vec3,
) -> EnemyPreset {
    create_enemy_preset(
        EnemyType::RedArremerKing,
        asset_server,
        texture_atlas_layouts,
        start_position,
        &Vec2::ZERO,
        &UVec2::new(76, 46),
        (38, 13),
        Collider::capsule(24.0, 40.0),
        Some(0.0),
    )
}
