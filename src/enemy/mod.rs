pub mod setup;
pub mod state;
pub mod update;

use super::enemy::update::update as enemy_update;
use bevy::prelude::{App, Plugin, Update};

pub struct EnemyPlugin;

impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, enemy_update);
    }
}
