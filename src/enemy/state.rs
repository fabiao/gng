use std::fmt;

use bevy::prelude::{Component, Vec2};

use crate::common::animated_sprite_state::AnimatedSpriteState;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum EnemyType {
    Skeleton,
    Zombie,
    Ghost,
    RedArremerKing,
}

impl fmt::Display for EnemyType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EnemyType::Skeleton => write!(f, "skeleton"),
            EnemyType::Zombie => write!(f, "zombie"),
            EnemyType::Ghost => write!(f, "ghost"),
            EnemyType::RedArremerKing => write!(f, "red_arremer_king"),
        }
    }
}

impl From<&str> for EnemyType {
    fn from(value: &str) -> Self {
        match value {
            "skeleton" => EnemyType::Skeleton,
            "zombie" => EnemyType::Zombie,
            "ghost" => EnemyType::Ghost,
            "red_arremer_king" => EnemyType::RedArremerKing,
            _ => EnemyType::Skeleton,
        }
    }
}

#[derive(Component, Clone)]
pub struct EnemyState {
    pub enemy_type: EnemyType,
    pub state: AnimatedSpriteState,
}

impl Default for EnemyState {
    fn default() -> Self {
        Self {
            enemy_type: EnemyType::Skeleton,
            state: AnimatedSpriteState {
                velocity_increment: Vec2::new(20.0, 0.0),
                min_velocity: Vec2::new(20.0, 0.0),
                max_velocity: Vec2::new(200.0, 0.0),
                ..AnimatedSpriteState::default()
            },
        }
    }
}

impl EnemyState {
    pub fn skeleton() -> Self {
        Self {
            enemy_type: EnemyType::Skeleton,
            state: AnimatedSpriteState {
                velocity_increment: Vec2::new(20.0, 0.0),
                min_velocity: Vec2::new(20.0, 0.0),
                max_velocity: Vec2::new(200.0, 0.0),
                ..AnimatedSpriteState::default()
            },
        }
    }

    pub fn zombie() -> Self {
        Self {
            enemy_type: EnemyType::Zombie,
            state: AnimatedSpriteState {
                velocity_increment: Vec2::new(19.0, 0.0),
                min_velocity: Vec2::new(15.0, 0.0),
                max_velocity: Vec2::new(160.0, 0.0),
                ..AnimatedSpriteState::default()
            },
        }
    }

    pub fn ghost() -> Self {
        Self {
            enemy_type: EnemyType::Ghost,
            state: AnimatedSpriteState {
                velocity_increment: Vec2::new(19.0, 0.0),
                min_velocity: Vec2::new(15.0, 0.0),
                max_velocity: Vec2::new(160.0, 0.0),
                ..AnimatedSpriteState::default()
            },
        }
    }

    pub fn red_arremer_king() -> Self {
        Self {
            enemy_type: EnemyType::RedArremerKing,
            state: AnimatedSpriteState {
                velocity_increment: Vec2::new(19.0, 0.0),
                min_velocity: Vec2::new(15.0, 0.0),
                max_velocity: Vec2::new(160.0, 0.0),
                ..AnimatedSpriteState::default()
            },
        }
    }
}
