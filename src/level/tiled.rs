use std::{
    io::{Cursor, ErrorKind},
    path::Path,
    sync::Arc,
};

use avian2d::prelude::{Collider, RigidBody, Sensor};
use bevy::{
    asset::{io::Reader, Asset, AssetLoader, AssetPath, LoadContext},
    ecs::event::{Event, EventWriter},
    prelude::{
        AssetEvent, AssetServer, Assets, Commands, Component, DespawnRecursiveExt, Entity,
        EventReader, GlobalTransform, Handle, Image, Query, Res, ResMut, Transform, Vec2, Vec3,
        With,
    },
    reflect::TypePath,
    sprite::TextureAtlasLayout,
    time::Time,
    utils::HashMap,
};
use bevy_ecs_tilemap::prelude::*;
use tiled::{
    Layer, LayerType, Loader, Map, Object, ObjectShape, Orientation, ResourceReader, TileId,
    TileLayer, Tileset,
};

use crate::{
    common::{
        animated_sprite_state::CollisionLayer,
        game_layer::{get_z_index, DrawLayer},
    },
    enemy::{
        setup::{
            create_ghost_preset, create_red_arremer_king_preset, create_skeleton_preset,
            create_zombie_preset,
        },
        state::{EnemyState, EnemyType},
        update::despawn_current_enemies,
    },
    input::{self, state::InputState},
    player::{
        setup::create_player_preset,
        state::{PlayerState, WeaponState},
        update::despawn_current_players_and_weapons,
    },
};

#[derive(TypePath, Asset, Debug)]
pub struct TiledMap {
    pub map: Map,
    pub tilemap_textures: HashMap<usize, TilemapTexture>,
    // The offset into the tileset_images for each tile id within each tileset
    pub tile_image_offsets: HashMap<(usize, TileId), u32>,
}

#[derive(Default, Component)]
pub struct TiledMapTiles {
    pub tiled_map_handle: Handle<TiledMap>,
    pub tiled_layer_storage: HashMap<u32, Entity>,
}

struct BytesResourceReader {
    bytes: Arc<[u8]>,
}

impl ResourceReader for BytesResourceReader {
    type Resource = Cursor<Arc<[u8]>>;
    type Error = std::io::Error;

    fn read_from(&mut self, _path: &Path) -> std::result::Result<Self::Resource, Self::Error> {
        // In this case, the path is ignored because the byte data is already provided.
        Ok(Cursor::new(self.bytes.clone()))
    }
}

#[derive(Default)]
pub struct TiledLoader;

impl AssetLoader for TiledLoader {
    type Asset = TiledMap;
    type Settings = ();
    type Error = std::io::Error;

    async fn load<'a>(
        &'a self,
        _reader: &mut dyn Reader,
        _settings: &Self::Settings,
        load_context: &mut LoadContext<'_>,
    ) -> Result<Self::Asset, Self::Error> {
        let map_asset_path_buffer = Path::new("assets/").join(load_context.path());
        let map_asset_path = AssetPath::from_path(map_asset_path_buffer.as_path());

        log::info!("Loading {:?}", map_asset_path.path());
        let mut loader = Loader::new();
        let map = loader.load_tmx_map(map_asset_path.path()).map_err(|e| {
            std::io::Error::new(ErrorKind::Other, format!("Could not load TMX map: {e}"))
        })?;

        let mut tilemap_textures = HashMap::default();
        let mut tile_image_offsets = HashMap::default();

        for (tileset_index, tileset) in map.tilesets().iter().enumerate() {
            let tilemap_texture = match &tileset.image {
                None => {
                    let mut tile_images: Vec<Handle<Image>> = Vec::new();
                    for (tile_id, tile) in tileset.tiles() {
                        if let Some(img) = &tile.image {
                            let canonicalized_source = img.source.canonicalize().unwrap();
                            log::info!("Loading tile image from {canonicalized_source:?} as image ({tileset_index}, {tile_id})");
                            let texture: Handle<Image> =
                                load_context.load(canonicalized_source);
                            tile_image_offsets
                                .insert((tileset_index, tile_id), tile_images.len() as u32);
                            tile_images.push(texture.clone());
                        }
                    }

                    TilemapTexture::Vector(tile_images)
                }
                Some(img) => {
                    let canonicalized_source = img.source.canonicalize().unwrap();
                    log::info!("Loading tile image from {canonicalized_source:?} as image ({tileset_index})");
                    let texture: Handle<Image> = load_context.load(canonicalized_source);

                    TilemapTexture::Single(texture.clone())
                }
            };

            tilemap_textures.insert(tileset_index, tilemap_texture);
        }

        let asset_map = TiledMap {
            map,
            tilemap_textures,
            tile_image_offsets,
        };

        log::info!("Loaded map: {}", load_context.path().display());
        Ok(asset_map)
    }

    fn extensions(&self) -> &[&str] {
        static EXTENSIONS: &[&str] = &["tmx"];
        EXTENSIONS
    }
}

fn map_points_and_reflect_y(points: &Vec<(f32, f32)>) -> Vec<Vec2> {
    points
        .iter()
        .rev()
        .map(|(x, y)| Vec2::new(*x, -*y))
        .collect()
}

fn create_tile_layer(
    commands: &mut Commands,
    tileset_index: usize,
    tileset: &Tileset,
    tiled_map: &TiledMap,
    tile_layer: &TileLayer,
    layer: &Layer,
    tilemap_texture: &TilemapTexture,
    layer_storage: &mut HashMap<u32, Entity>,
) {
    let TileLayer::Finite(layer_data) = tile_layer else {
        return;
    };

    let map_size = TilemapSize {
        x: tiled_map.map.width,
        y: tiled_map.map.height,
    };

    let grid_size = TilemapGridSize {
        x: tiled_map.map.tile_width as f32,
        y: tiled_map.map.tile_height as f32,
    };

    let map_type = match tiled_map.map.orientation {
        Orientation::Hexagonal => TilemapType::Hexagon(HexCoordSystem::Row),
        Orientation::Isometric => TilemapType::Isometric(IsoCoordSystem::Diamond),
        Orientation::Staggered => TilemapType::Isometric(IsoCoordSystem::Staggered),
        Orientation::Orthogonal => TilemapType::Square,
    };

    let mut tile_storage = TileStorage::empty(map_size);
    let layer_entity = commands.spawn_empty().id();

    for x in 0..map_size.x {
        for y in 0..map_size.y {
            // Transform TMX coords into bevy coords.
            let mapped_y = tiled_map.map.height - 1 - y;

            let mapped_x = x as i32;
            let mapped_y = mapped_y as i32;

            let layer_tile = match layer_data.get_tile(mapped_x, mapped_y) {
                Some(t) => t,
                None => {
                    continue;
                }
            };
            if tileset_index != layer_tile.tileset_index() {
                continue;
            }
            let layer_tile_data = match layer_data.get_tile_data(mapped_x, mapped_y) {
                Some(d) => d,
                None => {
                    continue;
                }
            };

            let texture_index = match tilemap_texture {
                TilemapTexture::Single(_) => layer_tile.id(),
                TilemapTexture::Vector(_) =>
                    *tiled_map.tile_image_offsets.get(&(tileset_index, layer_tile.id()))
                    .expect("The offset into to image vector should have been saved during the initial load."),
                _ => unreachable!()
            };

            let tile_pos = TilePos { x, y };
            let tile_entity = commands
                .spawn(TileBundle {
                    position: tile_pos,
                    tilemap_id: TilemapId(layer_entity),
                    texture_index: TileTextureIndex(texture_index),
                    flip: TileFlip {
                        x: layer_tile_data.flip_h,
                        y: layer_tile_data.flip_v,
                        d: layer_tile_data.flip_d,
                    },
                    ..Default::default()
                })
                .id();
            tile_storage.set(&tile_pos, tile_entity);
        }
    }

    let tile_size = TilemapTileSize {
        x: tileset.tile_width as f32,
        y: tileset.tile_height as f32,
    };

    let tile_spacing = TilemapSpacing {
        x: tileset.spacing as f32,
        y: tileset.spacing as f32,
    };

    let layer_index = layer.id();
    let layer_z_value = (layer_index - 1) as f32 / 100.0;

    log::info!("Creating layer with index {layer_index:?} and z value {layer_z_value:?}");

    commands.entity(layer_entity).insert(TilemapBundle {
        grid_size,
        size: map_size,
        storage: tile_storage,
        texture: tilemap_texture.clone(),
        tile_size,
        spacing: tile_spacing,
        transform: get_tilemap_center_transform(&map_size, &grid_size, &map_type, layer_z_value)
            * Transform::from_xyz(layer.offset_x, -layer.offset_y, 0.0),
        map_type,
        ..Default::default()
    });

    layer_storage.insert(layer_index, layer_entity);
}

fn create_level_colliders(
    commands: &mut Commands,
    o: &Object,
    layer: &Layer,
    map_transform: &Transform,
    object_layers_offset: &Vec3,
    layer_storage: &mut HashMap<u32, Entity>,
) {
    let user_type = o.user_type.as_str();

    match user_type {
        "ground" => match &o.shape {
            ObjectShape::Polygon { points } => {
                let reflected_points = map_points_and_reflect_y(&points);
                let position = Vec3::new(
                    map_transform.translation.x + o.x,
                    map_transform.translation.y - o.y,
                    map_transform.translation.z + get_z_index(DrawLayer::Object),
                );
                let local_transform = Transform::from_translation(position + *object_layers_offset);

                log::info!(
                    "Ground collider {} {}: {}",
                    o.name,
                    user_type,
                    local_transform.translation
                );

                let collider_entity = commands
                    .spawn((local_transform, GlobalTransform::default()))
                    .insert(RigidBody::Static)
                    .insert(CollisionLayer::Floor)
                    .insert(Collider::polyline(reflected_points, None))
                    .insert(TilemapType::Square)
                    .id();

                layer_storage.insert(layer.id(), collider_entity);
            }
            _ => {}
        },
        "ladder" => match &o.shape {
            ObjectShape::Polygon { points } => {
                let reflected_points = map_points_and_reflect_y(&points);
                let position = Vec3::new(
                    map_transform.translation.x + o.x,
                    map_transform.translation.y - o.y,
                    map_transform.translation.z + get_z_index(DrawLayer::Object),
                );
                let local_transform = Transform::from_translation(position + *object_layers_offset);

                log::info!(
                    "Ladder collider {} {}: {}",
                    o.name,
                    user_type,
                    local_transform.translation
                );

                let collider_entity = commands
                    .spawn((local_transform, GlobalTransform::default()))
                    .insert(Sensor)
                    .insert(CollisionLayer::Ladder)
                    .insert(Collider::polyline(reflected_points, None))
                    .insert(TilemapType::Square)
                    .id();

                layer_storage.insert(layer.id(), collider_entity);
            }
            _ => {}
        },
        _ => {}
    }
}

fn create_active_entities(
    commands: &mut Commands,
    o: &Object,
    map_transform: &Transform,
    object_layers_offset: &Vec3,
    asset_server: &Res<AssetServer>,
    texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
) {
    let user_type = o.user_type.as_str();
    let name = o.name.as_str();
    let position = Vec3::new(
        map_transform.translation.x + o.x,
        map_transform.translation.y - o.y,
        map_transform.translation.z + get_z_index(DrawLayer::Object),
    );
    let local_transform = Transform::from_translation(position + *object_layers_offset);
    let local_position = local_transform.translation;
    log::info!("Creating {} {} at: {}", name, user_type, local_position);
    match name {
        "player" => match &o.shape {
            ObjectShape::Point(_, _) => {
                let player_preset =
                    create_player_preset(&asset_server, texture_atlas_layouts, &local_position, Some(100.0));
                commands.spawn(player_preset);
            }
            _ => {
                log::error!("Undefined object shape {:?}!!", o.shape);
            }
        },
        "enemy" => match &o.shape {
            ObjectShape::Point(_, _) => {
                let enemy_type = EnemyType::from(user_type);
                let bundle = match enemy_type {
                    EnemyType::Skeleton => create_skeleton_preset(
                        &asset_server,
                        texture_atlas_layouts,
                        &local_position,
                    ),
                    EnemyType::Zombie => {
                        create_zombie_preset(&asset_server, texture_atlas_layouts, &local_position)
                    }
                    EnemyType::Ghost => {
                        create_ghost_preset(&asset_server, texture_atlas_layouts, &local_position)
                    }
                    EnemyType::RedArremerKing => create_red_arremer_king_preset(
                        &asset_server,
                        texture_atlas_layouts,
                        &local_position,
                    ),
                };
                commands.spawn(bundle);
            }
            _ => {
                log::error!("Undefined object shape {:?}!!", o.shape);
            }
        },
        _ => {
            log::error!("Undefined spawned object {}!!", name);
        }
    }
}

pub fn build_map(
    mut commands: &mut Commands,
    tiled_map: &TiledMap,
    mut tiled_layers_storage: &mut HashMap<u32, Entity>,
    asset_server: &Res<AssetServer>,
    mut texture_atlas_layouts: &mut ResMut<Assets<TextureAtlasLayout>>,
) {
    let tileset_layers = tiled_map
        .map
        .layers()
        .filter_map(|layer| match layer.layer_type() {
            LayerType::Tiles(tileset_layer) => Some((tileset_layer, layer)),
            _ => None,
        });
    let tilesets = tiled_map.map.tilesets();

    let mut tileset_counter = 0usize;
    {
        tileset_layers.for_each(|(tileset_layer, layer)| {
            if let Some(tileset) = tilesets.get(tileset_counter) {
                if let Some(tilemap_texture) = tiled_map.tilemap_textures.get(&tileset_counter) {
                    create_tile_layer(
                        &mut commands,
                        tileset_counter,
                        tileset,
                        tiled_map,
                        &tileset_layer,
                        &layer,
                        tilemap_texture,
                        &mut tiled_layers_storage,
                    );
                    tileset_counter += 1;
                }
            }
        });
    }

    let collision_objects = tiled_map
        .map
        .layers()
        .filter(|layer| layer.name == "colliders")
        .filter_map(|layer| match layer.layer_type() {
            LayerType::Objects(object_layer) => Some((object_layer, layer)),
            _ => None,
        });

    let tilemap_size = TilemapSize {
        x: tiled_map.map.width,
        y: tiled_map.map.height,
    };

    let grid_size = TilemapGridSize {
        x: tiled_map.map.tile_width as f32,
        y: tiled_map.map.tile_height as f32,
    };

    let map_orientation = tiled_map.map.orientation;

    let tilemap_type = match map_orientation {
        tiled::Orientation::Hexagonal => TilemapType::Hexagon(HexCoordSystem::Row),
        tiled::Orientation::Isometric => TilemapType::Isometric(IsoCoordSystem::Diamond),
        tiled::Orientation::Staggered => TilemapType::Isometric(IsoCoordSystem::Staggered),
        tiled::Orientation::Orthogonal => TilemapType::Square,
    };

    let map_transform = bevy_ecs_tilemap::helpers::geometry::get_tilemap_center_transform(
        &tilemap_size,
        &grid_size,
        &tilemap_type,
        get_z_index(DrawLayer::Background0),
    );

    let object_layers_offset_x = -(tiled_map.map.tile_width as f32 / 2.0);
    let object_layers_offset_y =
        tiled_map.map.height as f32 * (tiled_map.map.tile_height as f32 - 0.8);
    let object_layers_offset = Vec3::new(object_layers_offset_x, object_layers_offset_y, 0.0);

    collision_objects.for_each(|(object_layer, layer)| {
        let objects = object_layer.objects();
        for o in objects {
            create_level_colliders(
                &mut commands,
                &o,
                &layer,
                &map_transform,
                &object_layers_offset,
                &mut tiled_layers_storage,
            );
        }
    });

    let spawn_entities = tiled_map
        .map
        .layers()
        .filter(|layer| layer.name == "spawn_entities")
        .filter_map(|layer| match layer.layer_type() {
            LayerType::Objects(object_layer) => Some(object_layer),
            _ => None,
        });

    spawn_entities.for_each(|spawn_entity| {
        let objects = spawn_entity.objects();
        for o in objects {
            create_active_entities(
                &mut commands,
                &o,
                &map_transform,
                &object_layers_offset,
                &asset_server,
                &mut texture_atlas_layouts,
            );
        }
    });
}

pub fn remove_current_map_layers(
    commands: &mut Commands,
    tile_storage_query: &Query<(Entity, &TileStorage)>,
    tiled_layers_storage: &HashMap<u32, Entity>,
) {
    for layer_entity in tiled_layers_storage.values() {
        if let Ok((_, layer_tile_storage)) = tile_storage_query.get(*layer_entity) {
            for tile in layer_tile_storage.iter().flatten() {
                log::info!("Removing tile {:?}", *tile);
                commands.entity(*tile).despawn_recursive();
            }
        }

        log::info!("Removing layer entity {:?} recursively", *layer_entity);
        commands.entity(*layer_entity).despawn_recursive();
    }
}

pub fn process_map_events(
    mut commands: Commands,
    mut map_events: EventReader<AssetEvent<TiledMap>>,
    maps: Res<Assets<TiledMap>>,
    tile_storage_query: Query<(Entity, &TileStorage)>,
    mut tiled_map_tiles_query: Query<&mut TiledMapTiles>,
    players_query: Query<(Entity, &PlayerState)>,
    weapons_query: Query<(Entity, &WeaponState)>,
    enemies_query: Query<(Entity, &EnemyState)>,
    asset_server: Res<AssetServer>,
    mut texture_atlas_layouts: ResMut<Assets<TextureAtlasLayout>>,
) {
    for event in map_events.read() {
        match event {
            AssetEvent::LoadedWithDependencies { id } => {
                for mut tiled_map_tiles in tiled_map_tiles_query.iter_mut() {
                    if tiled_map_tiles.tiled_map_handle.id() == *id {
                        if let Some(tiled_map) = maps.get(*id) {
                            log::info!("Map {:?} loaded with dependencies!", *id);
                            build_map(
                                &mut commands,
                                tiled_map,
                                &mut tiled_map_tiles.tiled_layer_storage,
                                &asset_server,
                                &mut texture_atlas_layouts,
                            );
                        }
                    }
                }
            }
            AssetEvent::Added { id: _ } => {
                log::info!("Map added!");
                //changed_maps.push(*id);
            }
            AssetEvent::Modified { id: _ } => {
                log::info!("Map changed!");
                //changed_maps.push(*id);
            }
            AssetEvent::Removed { id } => {
                for tiled_map_tiles in tiled_map_tiles_query.iter_mut() {
                    if tiled_map_tiles.tiled_map_handle.id() == *id {
                        if let Some(_tiled_map) = maps.get(*id) {
                            remove_current_map_layers(
                                &mut commands,
                                &tile_storage_query,
                                &tiled_map_tiles.tiled_layer_storage,
                            );
                        }

                        despawn_current_players_and_weapons(
                            &mut commands,
                            &players_query,
                            &weapons_query,
                        );
                        despawn_current_enemies(&mut commands, &enemies_query)
                    }
                }
            }
            AssetEvent::Unused { id } => {}
        }
    }
}

pub fn update_scrolling(
    time: Res<Time>,
    input_state: Res<input::state::InputState>,
    mut query: Query<&mut Transform, With<TilemapType>>,
) {
    query.iter_mut().for_each(|mut transform| {
        let movement = input_state.check_max_movement();
        let movement = Vec2::X * movement.x * time.delta_secs() * 500.0;
        transform.translation -= Vec3::new(movement.x, movement.y, 0.0);
    });
}

fn spawn_tilemap_preset(
    commands: &mut Commands,
    tiled_map_tiles_query: &Query<&TiledMapTiles>,
    map_events: &mut EventWriter<AssetEvent<TiledMap>>,
    asset_server: &Res<AssetServer>,
    level_id: u32,
) {
    // TODO remove current level
    tiled_map_tiles_query.iter().for_each(|map_handle| {
        map_events.send(AssetEvent::Removed {
            id: map_handle.tiled_map_handle.id(),
        });
    });

    let path = format!("levels/level_{level_id:}/level.tmx");
    let map_handle: Handle<TiledMap> = asset_server.load(path);

    commands.spawn(TiledMapTiles {
        tiled_map_handle: map_handle,
        ..Default::default()
    });
}

#[derive(Event)]
pub struct ToggleLevelEvent(pub u32);

pub fn process_toggle_level_events(
    mut commands: Commands,
    tiled_map_tiles_query: Query<&TiledMapTiles>,
    asset_server: Res<AssetServer>,
    mut toggle_level_event_reader: EventReader<ToggleLevelEvent>,
    mut map_events: EventWriter<AssetEvent<TiledMap>>,
) {
    for ev in toggle_level_event_reader.read() {
        log::info!("Event level {:?}  triggered!", ev.0);
        spawn_tilemap_preset(
            &mut commands,
            &tiled_map_tiles_query,
            &mut map_events,
            &asset_server,
            ev.0,
        );
    }
}

pub fn process_check_level_events(
    mut toggle_level_event_writer: EventWriter<ToggleLevelEvent>,
    input_state: Res<InputState>,
) {
    if input_state.level_selected > 0 {
        toggle_level_event_writer.send(ToggleLevelEvent(input_state.level_selected));
    }
}
