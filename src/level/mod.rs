use bevy::{
    app::App, asset::AssetApp, ecs::schedule::IntoSystemConfigs, prelude::{Plugin, Update}
};
use bevy_ecs_tilemap::TilemapPlugin;

use self::tiled::{process_check_level_events, process_map_events, process_toggle_level_events, TiledLoader, TiledMap, ToggleLevelEvent};

pub mod tiled;

#[derive(Default)]
pub struct TiledMapPlugin;

impl Plugin for TiledMapPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(TilemapPlugin)
            .init_asset::<TiledMap>()
            .add_event::<ToggleLevelEvent>()
            .register_asset_loader(TiledLoader)
            .add_systems(Update, (process_map_events, process_check_level_events, process_toggle_level_events).chain());
    }
}
