use bevy::prelude::{App, Plugin};

pub mod collisions;
pub mod physics;

pub struct CollisionsAndPhysicsPlugin;

impl Plugin for CollisionsAndPhysicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((physics::PhysicsPlugin, collisions::CollisionsPlugin));
    }
}
