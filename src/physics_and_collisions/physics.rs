use crate::common::animated_sprite_state::CollisionLayer;
use bevy::prelude::*;
use avian2d::prelude::*;

#[derive(Bundle)]
pub struct SpritePhysicsAndCollisionsBundle {
    pub rigid_body: RigidBody,
    pub gravity_scale: GravityScale,
    pub velocity: LinearVelocity,
    pub collider_density: ColliderDensity,
    pub locked_axes: LockedAxes,
    pub collider: Collider,
    pub collision_layer: CollisionLayer,
    pub friction: Friction,
    pub restitution: Restitution,
    pub linear_damping: LinearDamping,
    pub dominance: Dominance,
    pub mass: Mass,
}

pub struct PhysicsPlugin;

impl Plugin for PhysicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((PhysicsPlugins::default(), PhysicsDebugPlugin::default()))
            .insert_resource(Gravity(Vec2::NEG_Y * 9.81 * 50.0));
    }
}
