use crate::{
    common::animated_sprite_state::{CollisionLayer, CollisionState},
    enemy::state::EnemyState,
    player::state::{PlayerState, WeaponState},
};
use bevy::prelude::*;
use avian2d::prelude::*;

fn player_start_colliding_with_world(
    player_state: &mut PlayerState,
    collision_state: CollisionState,
) {
    player_state.state.current_collision_state |= collision_state;
}

fn player_stop_colliding_with_world(
    player_state: &mut PlayerState,
    collision_state: CollisionState,
) {
    player_state.state.current_collision_state &= !collision_state;
}

fn player_start_colliding_with_enemy(player_state: &mut PlayerState, enemy_state: &mut EnemyState) {
    player_state.state.current_collision_state |= CollisionState::CollidingEnemy;
    enemy_state.state.current_collision_state |= CollisionState::CollidingPlayer;
}

fn player_stop_colliding_with_enemy(player_state: &mut PlayerState, enemy_state: &mut EnemyState) {
    player_state.state.current_collision_state &= !CollisionState::CollidingEnemy;
    enemy_state.state.current_collision_state &= !CollisionState::CollidingPlayer;
}

fn enemy_start_colliding_with_world(enemy_state: &mut EnemyState, collision_state: CollisionState) {
    enemy_state.state.current_collision_state |= collision_state;
}

fn enemy_stop_colliding_with_world(enemy_state: &mut EnemyState, collision_state: CollisionState) {
    enemy_state.state.current_collision_state &= !collision_state;
}

fn weapon_collided_with_enemy(weapon_state: &mut WeaponState, enemy_state: &mut EnemyState) {
    weapon_state.current_collision_state |= CollisionState::CollidingEnemy;
    enemy_state.state.current_collision_state |= CollisionState::CollidingDeadlyObject;
}

fn weapon_collided_with_world(weapon_state: &mut WeaponState, collision_state: CollisionState) {
    weapon_state.current_collision_state |= collision_state;
}

pub fn update_collisions(
    mut collision_started_events: EventReader<CollisionStarted>,
    mut collision_ended_events: EventReader<CollisionEnded>,
    mut player_query: Query<&mut PlayerState>,
    mut enemy_query: Query<&mut EnemyState>,
    mut weapon_query: Query<&mut WeaponState>,
    collision_query: Query<&CollisionLayer>,
) {
    for CollisionStarted(c1, c2) in collision_started_events.read() {
        if let Ok(c1_layer) = collision_query.get(*c1) {
            if let Ok(c2_layer) = collision_query.get(*c2) {
                match c1_layer {
                    CollisionLayer::Player => {
                        if let Ok(mut player_state) = player_query.get_mut(*c1) {
                            match c2_layer {
                                CollisionLayer::Enemy => {
                                    if let Ok(mut enemy_state) = enemy_query.get_mut(*c2) {
                                        player_start_colliding_with_enemy(
                                            &mut player_state,
                                            &mut enemy_state,
                                        );
                                    }
                                }
                                CollisionLayer::Floor => {
                                    player_start_colliding_with_world(
                                        &mut player_state,
                                        CollisionState::CollidingFloor,
                                    );
                                }
                                CollisionLayer::Ladder => {
                                    player_start_colliding_with_world(
                                        &mut player_state,
                                        CollisionState::CollidingLadder,
                                    );
                                }
                                _ => {}
                            }
                        }
                    }
                    CollisionLayer::Enemy => {
                        if let Ok(mut enemy_state) = enemy_query.get_mut(*c1) {
                            match c2_layer {
                                CollisionLayer::Player => {
                                    if let Ok(mut player_state) = player_query.get_mut(*c2) {
                                        player_start_colliding_with_enemy(
                                            &mut player_state,
                                            &mut enemy_state,
                                        );
                                    }
                                }
                                CollisionLayer::Weapon => {
                                    if let Ok(mut weapon_state) = weapon_query.get_mut(*c2) {
                                        weapon_collided_with_enemy(
                                            &mut weapon_state,
                                            &mut enemy_state,
                                        );
                                    }
                                }
                                CollisionLayer::Floor => {
                                    enemy_start_colliding_with_world(
                                        &mut enemy_state,
                                        CollisionState::CollidingFloor,
                                    );
                                }
                                CollisionLayer::Ladder => {
                                    enemy_start_colliding_with_world(
                                        &mut enemy_state,
                                        CollisionState::CollidingLadder,
                                    );
                                }
                                _ => {}
                            }
                        }
                    }
                    CollisionLayer::Floor => match c2_layer {
                        CollisionLayer::Player => {
                            if let Ok(mut player_state) = player_query.get_mut(*c2) {
                                player_start_colliding_with_world(
                                    &mut player_state,
                                    CollisionState::CollidingFloor,
                                );
                            }
                        }
                        CollisionLayer::Enemy => {
                            if let Ok(mut enemy_state) = enemy_query.get_mut(*c2) {
                                enemy_start_colliding_with_world(
                                    &mut enemy_state,
                                    CollisionState::CollidingFloor,
                                );
                            }
                        }
                        CollisionLayer::Weapon => {
                            if let Ok(mut weapon_state) = weapon_query.get_mut(*c2) {
                                weapon_collided_with_world(
                                    &mut weapon_state,
                                    CollisionState::CollidingFloor,
                                );
                            }
                        }
                        _ => {}
                    },
                    CollisionLayer::Ladder => match c2_layer {
                        CollisionLayer::Player => {
                            if let Ok(mut player_state) = player_query.get_mut(*c2) {
                                player_start_colliding_with_world(
                                    &mut player_state,
                                    CollisionState::CollidingLadder,
                                );
                            }
                        }
                        CollisionLayer::Enemy => {
                            if let Ok(mut enemy_state) = enemy_query.get_mut(*c2) {
                                enemy_start_colliding_with_world(
                                    &mut enemy_state,
                                    CollisionState::CollidingLadder,
                                );
                            }
                        }
                        _ => {}
                    },
                    _ => {}
                }
            }
        }
    }

    for CollisionEnded(c1, c2) in collision_ended_events.read() {
        if let Ok(c1_layer) = collision_query.get(*c1) {
            if let Ok(c2_layer) = collision_query.get(*c2) {
                match c1_layer {
                    CollisionLayer::Player => {
                        if let Ok(mut player_state) = player_query.get_mut(*c1) {
                            match c2_layer {
                                CollisionLayer::Enemy => {
                                    if let Ok(mut enemy_state) = enemy_query.get_mut(*c2) {
                                        player_stop_colliding_with_enemy(
                                            &mut player_state,
                                            &mut enemy_state,
                                        );
                                    }
                                }
                                CollisionLayer::Floor => {
                                    player_stop_colliding_with_world(
                                        &mut player_state,
                                        CollisionState::CollidingFloor,
                                    );
                                }
                                CollisionLayer::Ladder => {
                                    player_stop_colliding_with_world(
                                        &mut player_state,
                                        CollisionState::CollidingLadder,
                                    );
                                }
                                _ => {}
                            }
                        }
                    }
                    CollisionLayer::Enemy => {
                        if let Ok(mut enemy_state) = enemy_query.get_mut(*c1) {
                            match c2_layer {
                                CollisionLayer::Player => {
                                    if let Ok(mut player_state) = player_query.get_mut(*c2) {
                                        player_stop_colliding_with_enemy(
                                            &mut player_state,
                                            &mut enemy_state,
                                        );
                                    }
                                }
                                CollisionLayer::Floor => {
                                    enemy_stop_colliding_with_world(
                                        &mut enemy_state,
                                        CollisionState::CollidingFloor,
                                    );
                                }
                                CollisionLayer::Ladder => {
                                    enemy_stop_colliding_with_world(
                                        &mut enemy_state,
                                        CollisionState::CollidingLadder,
                                    );
                                }
                                _ => {}
                            }
                        }
                    }
                    CollisionLayer::Floor => match c2_layer {
                        CollisionLayer::Player => {
                            if let Ok(mut player_state) = player_query.get_mut(*c2) {
                                player_stop_colliding_with_world(
                                    &mut player_state,
                                    CollisionState::CollidingFloor,
                                );
                            }
                        }
                        CollisionLayer::Enemy => {
                            if let Ok(mut enemy_state) = enemy_query.get_mut(*c2) {
                                enemy_stop_colliding_with_world(
                                    &mut enemy_state,
                                    CollisionState::CollidingFloor,
                                );
                            }
                        }
                        _ => {}
                    },
                    CollisionLayer::Ladder => match c2_layer {
                        CollisionLayer::Player => {
                            if let Ok(mut player_state) = player_query.get_mut(*c2) {
                                player_stop_colliding_with_world(
                                    &mut player_state,
                                    CollisionState::CollidingLadder,
                                );
                            }
                        }
                        CollisionLayer::Enemy => {
                            if let Ok(mut enemy_state) = enemy_query.get_mut(*c2) {
                                enemy_stop_colliding_with_world(
                                    &mut enemy_state,
                                    CollisionState::CollidingLadder,
                                );
                            }
                        }
                        _ => {}
                    },
                    _ => {}
                }
            };
        };
    }
}

pub struct CollisionsPlugin;

impl Plugin for CollisionsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, update_collisions);
    }
}
