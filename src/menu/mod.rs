use crate::{
    game_state::GameState,
    level::tiled::ToggleLevelEvent,
    menu::{level_selector::LevelSelectorPlugin, settings::SettingsPlugin, splash::SplashPlugin},
};
use bevy::{app::AppExit, prelude::*};

pub mod level_selector;
pub mod settings;
pub mod splash;

// Tag component used to mark which setting is currently selected
#[derive(Component)]
pub struct SelectedOption;

// State used for the current menu screen
#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash, States)]
pub enum MenuState {
    Main,
    Settings,
    SettingsDisplay,
    SettingsSound,
    SelectLevel,
    #[default]
    Disabled,
}

// All actions that can be triggered from a button click
#[derive(Component, Debug, Clone, Copy)]
pub enum MenuButtonAction {
    SelectLevel,
    Level1,
    Level2,
    Level3,
    Level4,
    Level5,
    Level6,
    Level7,
    Level8,
    Level16,
    Settings,
    SettingsDisplay,
    SettingsSound,
    BackToMainMenu,
    BackToSelectLevel,
    BackToSettings,
    Quit,
}

impl MenuButtonAction {
    pub fn from(index: u32) -> MenuButtonAction {
        match index {
            1 => MenuButtonAction::Level1,
            2 => MenuButtonAction::Level2,
            3 => MenuButtonAction::Level3,
            4 => MenuButtonAction::Level4,
            5 => MenuButtonAction::Level5,
            6 => MenuButtonAction::Level6,
            7 => MenuButtonAction::Level7,
            8 => MenuButtonAction::Level8,
            16 => MenuButtonAction::Level16,
            _ => MenuButtonAction::SelectLevel,
        }
    }

    pub fn to(action: MenuButtonAction) -> u32 {
        match action {
            MenuButtonAction::Level1 => 1,
            MenuButtonAction::Level2 => 2,
            MenuButtonAction::Level3 => 3,
            MenuButtonAction::Level4 => 4,
            MenuButtonAction::Level5 => 5,
            MenuButtonAction::Level6 => 6,
            MenuButtonAction::Level7 => 7,
            MenuButtonAction::Level8 => 8,
            MenuButtonAction::Level16 => 16,
            _ => 0,
        }
    }
}

pub const NORMAL_BUTTON: Color = Color::srgb(0.15, 0.15, 0.15);
pub const HOVERED_BUTTON: Color = Color::srgb(0.25, 0.25, 0.25);
pub const HOVERED_PRESSED_BUTTON: Color = Color::srgb(0.25, 0.65, 0.25);
pub const PRESSED_BUTTON: Color = Color::srgb(0.35, 0.75, 0.35);
pub const TEXT_COLOR: Color = Color::srgb(0.9, 0.9, 0.9);

pub fn despawn_screen<T: Component>(to_despawn: Query<Entity, With<T>>, mut commands: Commands) {
    for entity in &to_despawn {
        commands.entity(entity).despawn_recursive();
    }
}

// This system handles changing all buttons color based on mouse interaction
fn button_system(
    mut interaction_query: Query<
        (&Interaction, &mut BackgroundColor, Option<&SelectedOption>),
        (Changed<Interaction>, With<Button>),
    >,
) {
    for (interaction, mut color, selected) in &mut interaction_query {
        *color = match (*interaction, selected) {
            (Interaction::Pressed, _) | (Interaction::None, Some(_)) => PRESSED_BUTTON.into(),
            (Interaction::Hovered, Some(_)) => HOVERED_PRESSED_BUTTON.into(),
            (Interaction::Hovered, None) => HOVERED_BUTTON.into(),
            (Interaction::None, None) => NORMAL_BUTTON.into(),
        }
    }
}

fn menu_setup(mut menu_state: ResMut<NextState<MenuState>>) {
    menu_state.set(MenuState::SelectLevel);
}

fn main_menu_setup(mut commands: Commands /*, asset_server: Res<AssetServer>*/) {
    commands.spawn((
        Node {
            width: Val::Percent(100.0),
            height: Val::Percent(100.0),
            align_items: AlignItems::Center,
            justify_content: JustifyContent::Center,
            ..default()
        },
        OnMainMenuScreen,
    ));
}

fn setup_level(
    menu_action: &MenuButtonAction,
    menu_state: &mut ResMut<NextState<MenuState>>,
    game_state: &mut ResMut<NextState<GameState>>,
    toggle_level_event_writer: &mut EventWriter<ToggleLevelEvent>,
) {
    game_state.set(GameState::Game);
    menu_state.set(MenuState::Disabled);
    let level = MenuButtonAction::to(*menu_action);
    toggle_level_event_writer.send(ToggleLevelEvent(level));
    log::info!("Level selected: {:?}", menu_action);
}

fn menu_action(
    interaction_query: Query<
        (&Interaction, &MenuButtonAction),
        (Changed<Interaction>, With<Button>),
    >,
    mut app_exit_events: EventWriter<AppExit>,
    mut menu_state: ResMut<NextState<MenuState>>,
    mut game_state: ResMut<NextState<GameState>>,
    mut toggle_level_event_writer: EventWriter<ToggleLevelEvent>,
) {
    for (interaction, menu_button_action) in &interaction_query {
        if *interaction == Interaction::Pressed {
            match menu_button_action {
                MenuButtonAction::Quit => {
                    app_exit_events.send(AppExit::Success);
                }
                MenuButtonAction::SelectLevel => menu_state.set(MenuState::SelectLevel),
                MenuButtonAction::Level1
                | MenuButtonAction::Level2
                | MenuButtonAction::Level3
                | MenuButtonAction::Level4
                | MenuButtonAction::Level5
                | MenuButtonAction::Level6
                | MenuButtonAction::Level7
                | MenuButtonAction::Level8
                | MenuButtonAction::Level16 => {
                    setup_level(
                        menu_button_action,
                        &mut menu_state,
                        &mut game_state,
                        &mut toggle_level_event_writer,
                    );
                }
                MenuButtonAction::Settings => menu_state.set(MenuState::Settings),
                MenuButtonAction::SettingsDisplay => {
                    menu_state.set(MenuState::SettingsDisplay);
                }
                MenuButtonAction::SettingsSound => {
                    menu_state.set(MenuState::SettingsSound);
                }
                MenuButtonAction::BackToMainMenu => menu_state.set(MenuState::Main),
                MenuButtonAction::BackToSelectLevel => {
                    menu_state.set(MenuState::SelectLevel);
                }
                MenuButtonAction::BackToSettings => {
                    menu_state.set(MenuState::Settings);
                }
            }
        }
    }
}

// Tag component used to tag entities added on the main menu screen
#[derive(Component)]
struct OnMainMenuScreen;

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<GameState>()
            .init_state::<MenuState>()
            .add_systems(OnEnter(GameState::Menu), menu_setup)
            // Systems to handle the main menu screen
            .add_systems(OnEnter(MenuState::Main), main_menu_setup)
            .add_systems(OnExit(MenuState::Main), despawn_screen::<OnMainMenuScreen>)
            .add_plugins(SplashPlugin)
            .add_plugins(SettingsPlugin)
            .add_plugins(LevelSelectorPlugin)
            // Common systems to all screens that handles buttons behavior
            .add_systems(
                Update,
                (menu_action, button_system).run_if(in_state(GameState::Menu)),
            );
    }
}
