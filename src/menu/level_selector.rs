use bevy::{color::palettes::css::CRIMSON, prelude::*};

use crate::menu::{despawn_screen, MenuButtonAction, MenuState, NORMAL_BUTTON, TEXT_COLOR};

pub struct LevelSelectorPlugin;

impl Plugin for LevelSelectorPlugin {
    fn build(&self, app: &mut App) {
        app
            // Systems to handle the settings menu screen
            .add_systems(OnEnter(MenuState::SelectLevel), select_level_menu_setup)
            .add_systems(
                OnExit(MenuState::SelectLevel),
                despawn_screen::<OnSelectLevelMenuScreen>,
            );
    }
}

// Tag component used to tag entities added on the settings menu screen
#[derive(Component)]
struct OnSelectLevelMenuScreen;

fn select_level_menu_setup(mut commands: Commands) {
    let button_style = Node {
        width: Val::Px(200.0),
        height: Val::Px(65.0),
        margin: UiRect::all(Val::Px(20.0)),
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..default()
    };

    let default_button_text_font = TextFont {
        font_size: 40.0,
        ..default()
    };

    commands
        .spawn((
            Node {
                width: Val::Percent(100.0),
                height: Val::Percent(100.0),
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                ..default()
            },
            OnSelectLevelMenuScreen,
        ))
        .with_children(|parent| {
            parent
                .spawn((
                    Node {
                        flex_direction: FlexDirection::Column,
                        align_items: AlignItems::Center,
                        overflow: Overflow::scroll_y(),
                        ..default()
                    },
                    BackgroundColor(Color::Srgba(CRIMSON)),
                ))
                .with_children(|parent| {
                    // Display the game name
                    parent.spawn((
                        Text::new("Levels"),
                        TextFont {
                            font_size: 80.0,
                            ..default()
                        },
                        TextLayout::new_with_justify(JustifyText::Center),
                        TextColor(TEXT_COLOR),
                    ));

                    for l in 1..8 {
                        parent
                            .spawn((
                                Button,
                                BackgroundColor(NORMAL_BUTTON),
                                button_style.clone(),
                                default_button_text_font.clone(),
                                MenuButtonAction::from(l))
                            )
                            .with_children(|parent| {
                                parent.spawn((
                                    Text::new(format!("Level {:}", l)),
                                    default_button_text_font.clone(),
                                ));
                            });
                    }

                    parent
                        .spawn((
                            Button,
                            BackgroundColor(NORMAL_BUTTON),
                            button_style.clone(),
                            default_button_text_font.clone(),
                            MenuButtonAction::from(16),
                        ))
                        .with_children(|parent| {
                            parent.spawn((
                                Text::new(format!("Level {:}", 16)),
                                default_button_text_font.clone(),
                            ));
                        });
                });
        });
}
