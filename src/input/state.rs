use bevy::{
    math::Vec2,
    prelude::{Component, Resource},
};

#[derive(Component, Debug, Copy, Clone, PartialEq)]
pub enum ButtonState {
    None,
    JustPressed,
    Pressing,
    JustReleased,
}

#[derive(Resource, Component, Copy, Clone, PartialEq, Debug)]
pub struct InputState {
    pub pad_connected: bool,
    pub step: f32,
    pub axes: Vec2,
    pub arrows: Vec2,
    pub shoot_state: ButtonState,
    pub jump_state: ButtonState,
    pub toggle_armor: bool,
    pub toggle_weapon: bool,
    pub respawn_player: bool,
    pub level_selected: u32,
}

impl InputState {
    pub fn check_max_movement(&self) -> Vec2 {
        let x = if self.axes.x.abs() > self.arrows.x.abs() {
            self.axes.x
        } else {
            self.arrows.x
        };
        let y = if self.axes.y.abs() > self.arrows.y.abs() {
            self.axes.y
        } else {
            self.arrows.y
        };
        Vec2::new(x, y)
    }

    pub fn check_moved(&self) -> (bool, bool) {
        let movement = self.check_max_movement();
        (movement.x.abs() >= self.step, movement.y.abs() >= self.step)
    }

    pub fn reset(&mut self) {
        self.axes = Vec2::ZERO;
        self.arrows = Vec2::ZERO;
        self.shoot_state = ButtonState::None;
        self.jump_state = ButtonState::None;
        self.toggle_armor = false;
        self.toggle_weapon = false;
        self.respawn_player = false;
        self.level_selected = 0;
    }
}

impl Default for InputState {
    fn default() -> Self {
        Self {
            pad_connected: false,
            step: 0.25,
            axes: Vec2::ZERO,
            arrows: Vec2::ZERO,
            shoot_state: ButtonState::None,
            jump_state: ButtonState::None,
            toggle_armor: false,
            toggle_weapon: false,
            respawn_player: false,
            level_selected: 0,
        }
    }
}
