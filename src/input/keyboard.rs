use super::state::{ButtonState, InputState};
use bevy::{
    app::AppExit,
    ecs::event::Events,
    input::{keyboard::KeyCode, ButtonInput},
    prelude::*,
};

pub fn check(
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut app_exit_events: ResMut<Events<AppExit>>,
    mut input_state: ResMut<InputState>,
) {
    if keyboard_input.pressed(KeyCode::Escape) {
        app_exit_events.send(AppExit::Success);
        return;
    }

    input_state.respawn_player = keyboard_input.just_released(KeyCode::KeyR);
    input_state.toggle_armor = keyboard_input.just_released(KeyCode::KeyA);
    input_state.toggle_weapon = keyboard_input.just_released(KeyCode::KeyW);

    if keyboard_input.just_released(KeyCode::Digit1) {
        input_state.level_selected = 1;
    }
    if keyboard_input.just_released(KeyCode::Digit2) {
        input_state.level_selected = 2;
    }
    if keyboard_input.just_released(KeyCode::Digit3) {
        input_state.level_selected = 3;
    }
    if keyboard_input.just_released(KeyCode::Digit4) {
        input_state.level_selected = 4;
    }
    if keyboard_input.just_released(KeyCode::Digit5) {
        input_state.level_selected = 5;
    }
    if keyboard_input.just_released(KeyCode::Digit6) {
        input_state.level_selected = 6;
    }
    if keyboard_input.just_released(KeyCode::Digit7) {
        input_state.level_selected = 7;
    }
    if keyboard_input.just_released(KeyCode::Digit8) {
        input_state.level_selected = 8;
    }
    if keyboard_input.just_released(KeyCode::Digit9) {
        input_state.level_selected = 16;
    }

    if keyboard_input.just_released(KeyCode::ArrowUp)
        || keyboard_input.just_released(KeyCode::ArrowDown)
    {
        input_state.arrows.y = 0.0;
    }
    if keyboard_input.just_released(KeyCode::ArrowLeft)
        || keyboard_input.just_released(KeyCode::ArrowRight)
    {
        input_state.arrows.x = 0.0;
    }

    if keyboard_input.pressed(KeyCode::ArrowUp) {
        input_state.arrows.y = (input_state.arrows.y + input_state.step).min(1.0);
    }
    if keyboard_input.pressed(KeyCode::ArrowRight) {
        input_state.arrows.x = (input_state.arrows.x + input_state.step).min(1.0);
    }
    if keyboard_input.pressed(KeyCode::ArrowDown) {
        input_state.arrows.y = (input_state.arrows.y - input_state.step).max(-1.0);
    }
    if keyboard_input.pressed(KeyCode::ArrowLeft) {
        input_state.arrows.x = (input_state.arrows.x - input_state.step).max(-1.0);
    }

    if keyboard_input.pressed(KeyCode::Space) {
        if keyboard_input.just_pressed(KeyCode::Space) {
            input_state.jump_state = ButtonState::JustPressed;
        } else {
            input_state.jump_state = ButtonState::Pressing;
        }
    } else if keyboard_input.just_released(KeyCode::Space) {
        input_state.jump_state = ButtonState::JustReleased;
    }

    if keyboard_input.pressed(KeyCode::ControlLeft) {
        if keyboard_input.just_pressed(KeyCode::ControlLeft) {
            input_state.shoot_state = ButtonState::JustPressed;
        } else {
            input_state.shoot_state = ButtonState::Pressing;
        }
    } else if keyboard_input.just_released(KeyCode::ControlLeft) {
        input_state.shoot_state = ButtonState::JustReleased;
    }
}
