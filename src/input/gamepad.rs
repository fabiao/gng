use super::state::{ButtonState, InputState};
use bevy::{app::AppExit, prelude::*};

#[derive(Resource, Clone, Debug, Reflect)]
#[reflect(Resource)]
pub struct GamepadConfig {
    min_speed: f32,
    max_speed: f32,
    threshold_drift_speed: f32,
}

impl Default for GamepadConfig {
    fn default() -> Self {
        GamepadConfig {
            min_speed: 0.0,
            max_speed: 1.0,
            threshold_drift_speed: 0.25,
        }
    }
}

pub fn check(
    mut app_exit_events: ResMut<Events<AppExit>>,
    gamepad_config: Res<GamepadConfig>,
    gamepads: Query<&Gamepad>,
    mut input_state: ResMut<InputState>,
) {
    for gamepad in gamepads.iter() {
        if gamepad.pressed(GamepadButton::Start) {
            app_exit_events.send(AppExit::Success);
            return;
        }

        if let Some(x) = gamepad.get(GamepadAxis::LeftStickX) {
            let x = x / 3.0;
            if x.abs() > gamepad_config.threshold_drift_speed {
                input_state.axes.x = x.signum() * x.abs().min(gamepad_config.max_speed);
            } else {
                input_state.axes.x = gamepad_config.min_speed;
            }
        }
        if let Some(y) = gamepad.get(GamepadAxis::LeftStickY) {
            let y = y / 3.0;
            if y.abs() > gamepad_config.threshold_drift_speed {
                input_state.axes.y = y.signum() * y.abs().min(gamepad_config.max_speed);
            } else {
                input_state.axes.y = gamepad_config.min_speed;
            }
        }

        if gamepad.just_released(GamepadButton::DPadUp)
            || gamepad.just_released(GamepadButton::DPadDown)
        {
            input_state.arrows.y = gamepad_config.min_speed;
        }
        if gamepad.just_released(GamepadButton::DPadRight)
            || gamepad.just_released(GamepadButton::DPadLeft)
        {
            input_state.arrows.x = gamepad_config.min_speed;
        }

        if gamepad.pressed(GamepadButton::DPadUp) {
            input_state.arrows.y =
                (input_state.arrows.y + input_state.step).min(gamepad_config.max_speed);
            /*if gamepad.just_pressed(GamepadButton::DPadUp) {
                log::info!("Pressing pad up");
            }*/
        }
        if gamepad.pressed(GamepadButton::DPadRight) {
            input_state.arrows.x =
                (input_state.arrows.x + input_state.step).min(gamepad_config.max_speed);
            /*if gamepad.just_pressed(GamepadButton::DPadRight) {
                log::info!("Pressing pad right");
            }*/
        }
        if gamepad.pressed(GamepadButton::DPadDown) {
            input_state.arrows.y =
                (input_state.arrows.y - input_state.step).max(-gamepad_config.max_speed);
            /*if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButton::DPadDown)) {
                log::info!("Pressing pad down");
            }*/
        }
        if gamepad.pressed(GamepadButton::DPadLeft) {
            input_state.arrows.x =
                (input_state.arrows.x - input_state.step).max(-gamepad_config.max_speed);
            /*if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButton::DPadLeft)) {
                log::info!("Pressing pad left");
            }*/
        }

        if gamepad.just_pressed(GamepadButton::South) {
            if gamepad.just_pressed(GamepadButton::South) {
                input_state.jump_state = ButtonState::JustPressed;
            } else {
                input_state.jump_state = ButtonState::Pressing;
            }
        } else if gamepad.just_released(GamepadButton::South) {
            input_state.jump_state = ButtonState::JustReleased;
        }

        if gamepad.pressed(GamepadButton::West) {
            if gamepad.just_pressed(GamepadButton::West) {
                input_state.shoot_state = ButtonState::JustPressed;
            } else {
                input_state.shoot_state = ButtonState::Pressing;
            }
        } else if gamepad.just_released(GamepadButton::West) {
            input_state.shoot_state = ButtonState::JustReleased;
        }
    }
}
