pub mod gamepad;
pub mod keyboard;
pub mod state;

use self::{gamepad::check as check_gamepad, keyboard::check as check_keyboard, state::InputState};
use bevy::prelude::*;
use gamepad::GamepadConfig;

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<InputState>()
            .init_resource::<GamepadConfig>()
            .add_systems(
                Update,
                (cleanup_inputs, check_keyboard, check_gamepad).chain(),
            );
    }
}

pub fn cleanup_inputs(mut input_state: ResMut<InputState>) {
    input_state.reset();
}
