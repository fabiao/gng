mod common;
mod enemy;
mod game_state;
mod input;
mod level;
mod menu;
mod physics_and_collisions;
mod player;
mod telemetries;

use bevy::{prelude::*, window::WindowResolution};
use bevy_inspector_egui::{bevy_egui::EguiPlugin, DefaultInspectorConfigPlugin};
use common::camera::PlayerCameraPlugin;
use enemy::EnemyPlugin;
use input::InputPlugin;
use level::TiledMapPlugin;
use micro_banimate::BanimatePluginGroup;
use physics_and_collisions::CollisionsAndPhysicsPlugin;
use player::PlayerPlugin;
use telemetries::TelemetryPlugin;
use menu::MenuPlugin;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .add_plugins(
            DefaultPlugins
                .set(AssetPlugin {
                    watch_for_changes_override: Some(true),
                    mode: AssetMode::Unprocessed,
                    ..default()
                })
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: String::from("Phys GnG"),
                        resolution: WindowResolution::new(1280.0, 720.0),
                        ..default()
                    }),
                    ..default()
                })
                .set(ImagePlugin::default_nearest()),
        )
        .add_plugins((EguiPlugin, DefaultInspectorConfigPlugin))
        .add_plugins(CollisionsAndPhysicsPlugin)
        .add_plugins(BanimatePluginGroup)
        .add_plugins(InputPlugin)
        .add_plugins(PlayerCameraPlugin)
        .add_plugins(TiledMapPlugin)
        .add_plugins(PlayerPlugin)
        .add_plugins(EnemyPlugin)
        .add_plugins(TelemetryPlugin)
        .add_plugins(MenuPlugin)
        .run();
}
