use bevy::prelude::*;

use crate::{input::state::InputState, player::state::PlayerState};

#[derive(Component, Clone)]
pub struct PlayerCameraState {}

impl Default for PlayerCameraState {
    fn default() -> Self {
        Self {}
    }
}

fn setup(mut commands: Commands) {
    commands
        .spawn(Camera2d::default())
        .insert(PlayerCameraState::default());
}

fn follow_player(
    time: Res<Time>,
    mut set: ParamSet<(
        Query<&Transform, With<PlayerState>>,
        Query<&mut Transform, With<PlayerCameraState>>,
    )>,
) {
    if let Ok(player_transform) = set.p0().get_single() {
        let player_translation = player_transform.translation;
        if let Ok(mut camera_transform) = set.p1().get_single_mut() {
            camera_transform.translation = camera_transform
                .translation
                .lerp(player_translation, time.delta_secs().max(0.1));
        }
    }
}

fn move_by_input(
    time: Res<Time>,
    mut query: Query<&mut Transform, With<PlayerCameraState>>,
    input_state: Res<InputState>,
) {
    let movement = input_state.check_max_movement();
    let input_movement = Vec2::X * movement.x * time.delta_secs();

    if let Some(mut camera_transform) = query.iter_mut().next() {
        let mut temp_position = camera_transform.translation;
        temp_position += Vec3::new(input_movement.x, input_movement.y, 0.0);
        camera_transform.translation = temp_position;

        //log::info!("Camera position: {:?}", camera_transform.translation);
    }
}

pub struct PlayerCameraPlugin;

impl Plugin for PlayerCameraPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_systems(Update, follow_player);
    }
}
