use core::fmt;

use bevy::{
    prelude::{Component, ReflectComponent, Vec2},
    reflect::Reflect,
    time::Timer,
};
use bitflags::bitflags;
use micro_banimate::directionality::Directionality;
use serde::{Deserialize, Serialize};

pub trait Actionable {
    fn check_current_action(&mut self, action: Action) -> bool;
    fn can_shoot(&self) -> bool;
    fn is_alive(&self) -> bool;
    fn can_move(&self) -> bool;
    fn get_current_actions(&self) -> &Action;
    //fn change_action(&mut self, action: Action);
    fn add_action(&mut self, action: Action);
    fn remove_action(&mut self, action: Action);
    fn check_action(&mut self, action: Action) -> bool;
}

#[derive(Clone, Component, Copy, Default, Debug, Eq, Hash, PartialEq, Reflect)]
#[reflect(Component)]
#[derive(Serialize, Deserialize)]
pub struct Action(u32);

bitflags! {
    impl Action: u32 {
        const None = 0b0000000000;
        const Grounded = 0b0000000001;
        const Jumping = 0b0000000010;
        const Climbing = 0b0000000100;
        const Shooting = 0b0000001000;
        const Falling = 0b0000010000;
        const Rising = 0b0000100000;
        const Dying = 0b0001000000;
        const Dead = 0b0010000000;
        const Crouched = 0b0100000000;
    }
}

#[derive(Clone, Component, Copy, Default, Debug, Eq, Hash, PartialEq, Reflect)]
#[reflect(Component)]
#[derive(Serialize, Deserialize)]
pub struct CollisionState(u32);

bitflags! {
    impl CollisionState: u32 {
        const NoColliding = 0b00000000;
        const CollidingFloor = 0b00000001;
        const CollidingLadder = 0b00000010;
        const CollidingEnemy = 0b00000100;
        const CollidingDeadlyObject = 0b00001000;
        const CollidingPlayer = 0b00010000;
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Component, Debug)]
pub enum CollisionLayer {
    Floor,
    Ladder,
    Enemy,
    Player,
    Weapon,
}

#[derive(Component, Clone)]
pub struct AnimatedSpriteState {
    pub current_action: Action,
    pub current_collision_state: CollisionState,
    pub velocity_increment: Vec2,
    pub max_velocity: Vec2,
    pub min_velocity: Vec2,
    pub animation_timer: Timer,
    pub animation_speed: f32,
}

impl Actionable for AnimatedSpriteState {
    fn check_current_action(&mut self, action: Action) -> bool {
        return self.current_action.intersects(action);
    }

    fn can_shoot(&self) -> bool {
        self.current_action.intersects(Action::Grounded)
            || self.current_action.intersects(Action::Jumping)
    }

    fn is_alive(&self) -> bool {
        !self.current_action.intersects(Action::Dying)
            && !self.current_action.intersects(Action::Dead)
    }

    fn can_move(&self) -> bool {
        self.is_alive() && !self.current_action.intersects(Action::Falling)
    }

    fn get_current_actions(&self) -> &Action {
        &self.current_action
    }

    /*fn change_action(&mut self, action: Action) {
        self.current_action = action;
    }*/

    fn add_action(&mut self, action: Action) {
        self.current_action |= action;
    }

    fn remove_action(&mut self, action: Action) {
        self.current_action &= !action;
    }

    fn check_action(&mut self, action: Action) -> bool {
        self.current_action.intersects(action)
    }
}

impl Default for AnimatedSpriteState {
    fn default() -> Self {
        Self {
            current_action: Action::None,
            current_collision_state: CollisionState::NoColliding,
            min_velocity: Vec2::ZERO,
            max_velocity: Vec2::ZERO,
            velocity_increment: Vec2::ZERO,
            animation_timer: Timer::default(),
            animation_speed: 1.0,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum AnimationType {
    None,
    Idle,
    Walk,
    Jump,
    Climb,
    Crouch,
    Fall,
    Rise,
    Die,
    Shoot,
    ShootCrouched,
}

impl fmt::Display for AnimationType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AnimationType::None => write!(f, "none"),
            AnimationType::Idle => write!(f, "idle"),
            AnimationType::Walk => write!(f, "walk"),
            AnimationType::Jump => write!(f, "jump"),
            AnimationType::Climb => write!(f, "climb"),
            AnimationType::Crouch => write!(f, "crouch"),
            AnimationType::Fall => write!(f, "fall"),
            AnimationType::Rise => write!(f, "rise"),
            AnimationType::Die => write!(f, "die"),
            AnimationType::Shoot => write!(f, "shoot"),
            AnimationType::ShootCrouched => write!(f, "shoot_crouched"),
        }
    }
}

pub trait PositionChecked {
    fn is_left(&self) -> bool;
	fn is_right(&self) -> bool;
	fn is_up(&self) -> bool;
	fn is_down(&self) -> bool;
    fn from_direction(&mut self, direction: &Vec2);
}

impl PositionChecked for Directionality {
	fn is_left(&self) -> bool {
		*self == Self::LeftDown || *self == Self::Left || *self == Self::LeftUp
	}

	fn is_right(&self) -> bool {
		*self == Self::RightDown || *self == Self::Right || *self == Self::RightUp
	}

	fn is_up(&self) -> bool {
		*self == Self::LeftUp || *self == Self::Up || *self == Self::RightUp
	}

	fn is_down(&self) -> bool {
		*self == Self::LeftDown || *self == Self::Down || *self == Self::RightDown
	}

    fn from_direction(&mut self, direction: &Vec2) {
        *self = if direction.x > 0.0 {
            if direction.y > 0.0 {
                Self::RightUp
            } else if direction.y < 0.0 {
                Self::RightDown
            } else {
                Self::Right
            }
        } else if direction.x < 0.0 {
            if direction.y > 0.0 {
                Self::LeftUp
            } else if direction.y < 0.0 {
                Self::LeftDown
            } else {
                Self::Left
            }
        } else {
            *self
        }
	}
}
