use bevy::{prelude::*, window::PrimaryWindow};
use bevy_inspector_egui::{bevy_egui::EguiContext, egui};
use avian2d::prelude::{LinearVelocity, RigidBody};
use micro_banimate::{definitions::{AnimationMode, AnimationStatus}, directionality::Directionality};

use crate::{
    common::animated_sprite_state::{Actionable, CollisionState},
    enemy::state::EnemyState,
    game_state::GameState,
    input::state::InputState,
    menu::MenuState,
    player::state::{PlayerState, WeaponState},
};

pub struct TelemetryPlugin;

impl Plugin for TelemetryPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, inspector_ui);
    }
}

pub fn inspector_ui(
    mut egui_query: Query<&mut EguiContext, With<PrimaryWindow>>,
    input_state: Res<InputState>,
    game_state: Res<State<GameState>>,
    menu_state: Res<State<MenuState>>,
    player_query: Query<(
        &LinearVelocity,
        &Transform,
        &PlayerState,
        &AnimationStatus,
        &AnimationMode,
        &RigidBody,
        &Directionality
    )>,
    weapons_query: Query<(&WeaponState, &Transform, &AnimationStatus, &AnimationMode)>,
    /*enemy_query: Query<(
        &LinearVelocity,
        &Transform,
        &EnemyState,
        &AnimationStatus,
        &AnimationMode,
        &RigidBody,
    )>,*/
    //floor_query: Query<(&CollisionLayer,&Transform)>,
) {
    if let Ok(mut egui_context) = egui_query.get_single_mut() {
        egui::Window::new("Debug UI").show(egui_context.get_mut(), |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.heading(format!("Game state: {:?}", game_state.get()));
                ui.label(format!("Menu state: {:?}", menu_state.get()));
                ui.label(format!("Level selected: {:?}", input_state.level_selected));
                /*ui.label("Arrows");
                ui.label(format!("{:?}", input_state.arrows));
                ui.label("Axes");
                ui.label(format!("{:?}", input_state.axes));
                ui.label("Jump");
                ui.label(format!("{:?}", input_state.jump_state));
                ui.label("Shoot");
                ui.label(format!("{:?}", input_state.shoot_state));*/

                let mut player_counter = 1;
                player_query.iter().for_each(
                    |(
                        velocity,
                        transform,
                        player_state,
                        animation_status,
                        animation_mode,
                        _rigid_body,
                        directionality
                    )| {
                        let current_actions = player_state.state.get_current_actions();

                        ui.heading(format!("Player {}", player_counter));
                        ui.label("Current actions");
                        ui.label(format!("{:?}", current_actions));
                        ui.label("Animation");
                        ui.label(format!(
                            "{}: {:?}",
                            //"{} {:?}: {} {}",
                            animation_status.active_name,
                            animation_mode,
                            //animation_status.active_step,
                            //animation_status.frame_time,
                            //player_state.state.animation_speed,
                        ));
                        ui.label("Direction");
                        ui.label(format!(
                            "{} {:.1} {:.1}",
                            directionality, player_state.direction.x, player_state.direction.y,
                        ));
                        ui.label("Velocity");
                        ui.label(format!("{:.1} {:.1}", velocity.x, velocity.y,));
                        //ui.label("Rigid body");
                        //ui.label(format!("{:?}", rigid_body));
                        ui.label("Position");
                        ui.label(format!(
                            "{:.1} {:.1} {:.1}",
                            transform.translation.x,
                            transform.translation.y,
                            transform.translation.z
                        ));
                        if player_state.state.current_collision_state == CollisionState::NoColliding
                        {
                            ui.label("No colliding");
                        }
                        if player_state
                            .state
                            .current_collision_state
                            .intersects(CollisionState::CollidingFloor)
                        {
                            ui.label("Colliding floor");
                        }
                        if player_state
                            .state
                            .current_collision_state
                            .intersects(CollisionState::CollidingLadder)
                        {
                            ui.label("Colliding ladder");
                        }
                        if player_state
                            .state
                            .current_collision_state
                            .intersects(CollisionState::CollidingEnemy)
                        {
                            ui.label("Colliding enemy");
                        }

                        player_counter += 1;
                    },
                );

                let mut weapon_counter = 1;
                weapons_query.iter().for_each(
                    |(weapon_state, transform, animation_status, animation_mode)| {
                        ui.heading(format!("Weapon {}", weapon_counter));
                        ui.label("Animation");
                        ui.label(format!(
                            "{}: {:?}",
                            //"{} {:?}: {} {}",
                            animation_status.active_name,
                            animation_mode,
                            //animation_status.active_step,
                            //animation_status.frame_time,
                            //player_state.state.animation_speed,
                        ));
                        ui.label("Direction");
                        ui.label(format!(
                            "{:.1} {:.1}",
                            weapon_state.direction.x, weapon_state.direction.y,
                        ));
                        ui.label("Position");
                        ui.label(format!(
                            "{:.1} {:.1} {:.1}",
                            transform.translation.x,
                            transform.translation.y,
                            transform.translation.z
                        ));

                        weapon_counter += 1;
                    },
                );

                /*let mut enemy_counter = 1;
                enemy_query.for_each(
                    |(velocity, transform, enemy_state, animation_status, animation_mode, rigid_body)| {
                        let current_actions = enemy_state.state.get_current_actions();

                        ui.heading(format!("Enemy {}", enemy_counter));
                        ui.label("Current action");
                        ui.label(format!("{:?}", current_actions));
                        ui.label("Animation");
                        ui.label(format!(
                            "{} {} {}",
                            animation_status.active_name,
                            animation_status.active_step,
                            animation_status.frame_time
                        ));
                        ui.label("Velocity");
                        ui.label(format!("{:.2} {:.2}", velocity.linvel.x, velocity.linvel.y));
                        ui.label("Rigid body");
                        ui.label(format!("{:?}", rigid_body));
                        ui.label("Position");
                        ui.label(format!(
                            "{:.2} {:.2} {:.2}",
                            transform.translation.x,
                            transform.translation.y,
                            transform.translation.z
                        ));
                        if enemy_state
                            .state
                            .current_collision_state
                            .intersects(CollisionState::CollidingFloor)
                        {
                            ui.label("Colliding floor");
                        }
                        if enemy_state
                            .state
                            .current_collision_state
                            .intersects(CollisionState::CollidingLadder)
                        {
                            ui.label("Colliding ladder");
                        }
                        if enemy_state
                            .state
                            .current_collision_state
                            .intersects(CollisionState::CollidingPlayer)
                        {
                            ui.label("Colliding player");
                        }
                        enemy_counter += 1;
                    },
                );*/

                /*let mut floor_counter = 1;
                let mut ladder_counter = 1;
                for (layer, transform) in floor_query.iter() {
                    match layer {
                        CollisionLayer::Floor => {
                            ui.heading(format!("Floor {}", floor_counter));
                            ui.label("Position");
                            ui.label(format!(
                                "{:.2} {:.2} {:.2}",
                                transform.translation.x,
                                transform.translation.y,
                                transform.translation.z
                            ));
                            floor_counter += 1;
                        }
                        CollisionLayer::Ladder => {
                            ui.heading(format!("Ladder {}", ladder_counter));
                            ui.label("Position");
                            ui.label(format!(
                                "{:.2} {:.2} {:.2}",
                                transform.translation.x,
                                transform.translation.y,
                                transform.translation.z
                            ));
                            ladder_counter += 1;
                        }
                        _ => {}
                    }
                }*/
            });
        });
    }
}
